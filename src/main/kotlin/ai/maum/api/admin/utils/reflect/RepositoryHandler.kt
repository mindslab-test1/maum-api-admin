package ai.maum.api.admin.utils.reflect

import ai.maum.api.admin.infra.repository.*
import ai.maum.api.admin.utils.enum.Engines
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Repository
import org.springframework.web.server.ResponseStatusException

@Repository
class RepositoryHandler(
        private val ttsModelRepository: TtsModelRepository,
        private val ttsValidRepository: TtsValidRepository,
        private val sttModelRepository: SttModelRepository,
        private val sttValidRepository: SttValidRepository,
        private val lipsyncModelRepository: LipsyncModelRepository,
        private val lipsyncValidRepository: LipsyncValidRepository,
){
    fun getModelRepository(engine: Engines): ModelRepository {
        return when(engine){
            Engines.TTS -> ttsModelRepository
            Engines.STT -> sttModelRepository
            Engines.LIPSYNC -> lipsyncModelRepository
            else -> throw ResponseStatusException(HttpStatus.NOT_IMPLEMENTED)
        }
    }

    fun getValidRepository(engine: Engines): ValidRepository {
        return when(engine){
            Engines.TTS -> ttsValidRepository
            Engines.STT -> sttValidRepository
            Engines.LIPSYNC -> lipsyncValidRepository
            else -> throw ResponseStatusException(HttpStatus.NOT_IMPLEMENTED)
        }
    }
}