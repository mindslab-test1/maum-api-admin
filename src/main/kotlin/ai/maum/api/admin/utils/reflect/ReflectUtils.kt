package ai.maum.api.admin.utils.reflect

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties

val logger: Logger = LoggerFactory.getLogger("ai.maum.api.admin.utils.reflect.ReflectUtils")

inline fun <reified T : Any> getPropByName(variable: T, name: String): Any? =
        variable.javaClass.kotlin.memberProperties.first{ it.name == name }.get(variable)

inline fun <reified T : Any> setPropByName(variable: T, name: String, value: Any) {
    val targetProperty = variable::class.memberProperties.first { it.name == name }
    if(targetProperty is KMutableProperty<*>)
        targetProperty.setter.call(variable, value)
    else logger.warn("set property by name failure")
}

inline fun <reified T : Any> invokeInterfaceMethod(interFace: T, name: String, vararg args: Any?): Any? =
        interFace::class.memberFunctions.first{ it.name == name }.call(interFace, *args)

