package ai.maum.api.admin.utils.enum

enum class Engines(val value: String, val props: List<String>) {
    TTS("tts", listOf("sampleRate", "speakerId", "lang")),
    STT("stt", listOf("sampleRate", "lang")),
    LIPSYNC("lipsync", listOf());
}