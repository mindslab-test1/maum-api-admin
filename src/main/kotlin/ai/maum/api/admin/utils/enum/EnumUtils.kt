package ai.maum.api.admin.utils.enum

import ai.maum.api.admin.utils.const.ENGINE_HEADER
import org.springframework.http.HttpHeaders

inline fun <reified T : Enum<*>> enumValueOrNull(name: String): T? =
        T::class.java.enumConstants.firstOrNull { it.name == name }

fun getEngineFromHeader(httpHeader: HttpHeaders): Engines? {
    return enumValueOrNull(httpHeader[ENGINE_HEADER]?.get(0).toString().toUpperCase())
}