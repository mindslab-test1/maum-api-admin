package ai.maum.api.admin.utils.json

import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive


fun mapToJsonObject(modelInfo: Map<String, Any?>): JsonObject {
    val modelInfoMap = mutableMapOf<String, JsonElement>()
    for (entry in modelInfo) {
        entry.value ?: continue
        modelInfoMap[entry.key] = when(entry.value){
            is Number -> JsonPrimitive(entry.value as Number)
            is String -> JsonPrimitive(entry.value as String)
            is Boolean -> JsonPrimitive(entry.value as Boolean)
            else -> throw Exception("Not json primitive")
        }
    }
    return JsonObject(modelInfoMap)
}