package ai.maum.api.admin.utils.reflect

import ai.maum.api.admin.infra.entity.*
import ai.maum.api.admin.utils.const.PARAM_HOST
import ai.maum.api.admin.utils.const.PARAM_OPEN
import ai.maum.api.admin.utils.const.PARAM_PORT
import ai.maum.api.admin.utils.enum.Engines
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException


@Component
class EntityHandler{
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val defaults = listOf(PARAM_HOST, PARAM_PORT, PARAM_OPEN)

    fun createNewModelEntity(params: Map<String, Any?>, engine: Engines): Any{
        val entity = when(engine){
            Engines.TTS -> TtsModelEntity()
            Engines.STT -> SttModelEntity()
            Engines.LIPSYNC -> LipsyncModelEntity()
            else -> throw ResponseStatusException(HttpStatus.NOT_IMPLEMENTED)
        }

        defaults.forEach { prop ->
            val propValue = params[prop] ?: return@forEach
            logger.debug("default prop: $prop = $propValue")
            setPropByName(entity, prop, propValue)
            logger.debug("prop set: ${getPropByName(entity, prop)}")
        }

        engine.props.forEach { prop ->
            val propValue = params[prop] ?: return@forEach
            logger.debug("custom prop: $prop = $propValue")
            setPropByName(entity, prop, propValue)
            logger.debug("prop set: ${getPropByName(entity, prop)}")
        }

        return entity
    }

    fun createNewValidEntityInstance(engine: Engines): Any{
        return when(engine){
            Engines.TTS -> TtsValidEntity()
            Engines.STT -> SttValidEntity()
            Engines.LIPSYNC -> LipsyncValidEntity()
            else -> throw ResponseStatusException(HttpStatus.NOT_IMPLEMENTED)
        }
    }

    fun modelInfoToMap(modelEntity: Any, engine: Engines): MutableMap<String, Any> {
        val modelInfoMap = mutableMapOf<String, Any>()
        defaults.forEach{ prop ->
            val propValue = getPropByName(modelEntity, prop) ?: TODO()
            logger.debug("default prop: $prop = $propValue")
            modelInfoMap[prop] = propValue
        }

        engine.props.forEach{ prop ->
            val propValue = getPropByName(modelEntity, prop) ?: TODO()
            logger.debug("custom prop: $prop = $propValue")
            modelInfoMap[prop] = propValue
        }
        return modelInfoMap
    }
}
