package ai.maum.api.admin.utils.const

const val FUN_SAVE = "save"
const val PARAM_HOST = "host"
const val PARAM_PORT = "port"
const val PARAM_OPEN = "open"
const val PARAM_MODEL_NAME = "modelName"
const val PARAM_MODEL_ENTITY = "model"
const val PARAM_ACTIVE = "active"
const val PARAM_ARRESTED = "arrested"
const val PARAM_CLIENT_ENTITY = "client"