package ai.maum.api.admin.boundaries.dto


data class ResponseDto(
        val status: Int? = 0,
        val message: String? = "Success",
        var payload: Map<String, Any>? = null,
)