package ai.maum.api.admin.boundaries.dto.model

data class ModelUpdateDto(
        val modelInfo: Map<String, Any?>,
        val modelName: String
)