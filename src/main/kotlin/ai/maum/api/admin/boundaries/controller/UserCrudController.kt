package ai.maum.api.admin.boundaries.controller

import ai.maum.api.admin.boundaries.dto.ResponseDto
import ai.maum.api.admin.boundaries.dto.user.UserUpdateDto
import ai.maum.api.admin.core.usecase.UserCrudUseCase
import ai.maum.api.admin.utils.enum.getEngineFromHeader
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("admin/api/user")
class UserCrudController (
        val userCrudUseCase: UserCrudUseCase
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val successEntity = ResponseEntity.ok(ResponseDto())

    @PutMapping(
            "add"
    )
    fun userAdd(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: UserUpdateDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("add user request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        userCrudUseCase.userAdd(dto = dto, engine = engine)
        return successEntity
    }

    @PostMapping(
            "arrest"
    )
    fun userArrest(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: UserUpdateDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("arrest user request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        userCrudUseCase.userArrest(dto = dto, engine = engine)
        return successEntity
    }

    @PostMapping(
            "release"
    )
    fun userRelease(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: UserUpdateDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("release user request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        userCrudUseCase.userRelease(dto = dto, engine = engine)
        return successEntity
    }

    @DeleteMapping(
            "remove"
    )
    fun userRemove(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: UserUpdateDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("remove user request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        userCrudUseCase.userRemove(dto = dto, engine = engine)
        return successEntity
    }
}