package ai.maum.api.admin.boundaries.controller

import ai.maum.api.admin.boundaries.dto.model.ModelCreateDto
import ai.maum.api.admin.boundaries.dto.model.ModelDeleteDto
import ai.maum.api.admin.boundaries.dto.model.ModelUpdateDto
import ai.maum.api.admin.boundaries.dto.ResponseDto
import ai.maum.api.admin.core.usecase.ModelCrudUseCase
import ai.maum.api.admin.utils.enum.*
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("admin/api/model")
class ModelCrudController (
        val modelCrudUseCase: ModelCrudUseCase
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val successEntity = ResponseEntity.ok(ResponseDto())

    @PutMapping(
            "/create"
    )
    fun modelCreate(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: ModelCreateDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("model create request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        modelCrudUseCase.modelCreate(dto = dto, engine = engine)
        return successEntity
    }

    @PostMapping(
            "/update"
    )
    fun modelUpdate(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: ModelUpdateDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("model update request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        modelCrudUseCase.modelUpdate(dto = dto, engine = engine)
        return successEntity
    }

    @DeleteMapping(
            "/delete"
    )
    fun modelDelete(
            @RequestHeader httpHeader: HttpHeaders,
            @RequestBody dto: ModelDeleteDto
    ): ResponseEntity<ResponseDto> {
        logger.debug("model delete request")
        val engine = getEngineFromHeader(httpHeader = httpHeader)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "engine enum")
        modelCrudUseCase.modelDelete(dto = dto, engine = engine)
        return successEntity
    }
}