package ai.maum.api.admin.boundaries.dto.model

data class ModelCreateDto(
        val modelInfo: Map<String, Any?>,
        val modelName: String,
        val creator: String,
)