package ai.maum.api.admin.boundaries

import ai.maum.api.admin.boundaries.dto.model.ModelCreateDto
import ai.maum.api.admin.core.model.UpdateMessage
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.FanoutExchange
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/admin/test")
class TestController(
        val exchangeMap: Map<String, FanoutExchange>
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @GetMapping("exchanges")
    fun testExchanges(@RequestHeader httpHeaders: HttpHeaders): ResponseEntity<Unit> {
        logger.info(exchangeMap.toString())

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }

    @PostMapping("jsonPrimitive")
    fun testJsonPrimitive(@RequestHeader httpHeaders: HttpHeaders, @RequestBody modelCreateDto: ModelCreateDto): ResponseEntity<Unit> {
        logger.info(modelCreateDto.toString())

        val modelInfoMap = mutableMapOf<String, JsonElement>()
        for(entry in modelCreateDto.modelInfo.entries){
            entry.value ?: continue
            modelInfoMap[entry.key] = when(entry.value){
                is Number -> JsonPrimitive(entry.value as Number)
                is String -> JsonPrimitive(entry.value as String)
                is Boolean -> JsonPrimitive(entry.value as Boolean)
                else -> throw Exception("Not json primitive")
            }
        }
        val message = UpdateMessage(
                modelInfo = JsonObject(modelInfoMap)
        )
        logger.info(message.toString())

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }
}