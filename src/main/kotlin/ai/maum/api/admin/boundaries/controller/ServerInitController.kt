package ai.maum.api.admin.boundaries.controller

import ai.maum.api.admin.boundaries.dto.ResponseDto
import ai.maum.api.admin.core.usecase.ModelCrudUseCase
import ai.maum.api.admin.utils.enum.Engines
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.lang.IllegalArgumentException

@RestController
@RequestMapping("admin/api/init")
class ServerInitController (
        val modelCrudUseCase: ModelCrudUseCase
){
    @GetMapping("getInitialData")
    fun initServer(@RequestHeader httpHeader: HttpHeaders): ResponseEntity<ResponseDto>{
        val responseDto = ResponseDto()
        try {
            val engine = Engines.valueOf(httpHeader["ai-maum-engine"]?.get(0).toString().toUpperCase())
            responseDto.payload = modelCrudUseCase.getInitData(
                    engines = engine
            )
            return ResponseEntity.ok(responseDto)
        } catch (e: IllegalArgumentException){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "enum")
        }

    }
}