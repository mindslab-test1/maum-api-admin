package ai.maum.api.admin.boundaries.dto.model

data class ModelDeleteDto(
        val modelName: String
)