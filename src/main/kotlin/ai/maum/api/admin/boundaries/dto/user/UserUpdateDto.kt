package ai.maum.api.admin.boundaries.dto.user

import kotlinx.serialization.Serializable

@Serializable
data class UserUpdateDto(
        val clientId: String,
        val modelName: String
)