package ai.maum.api.admin.boundaries.dto.init

data class ModelAndUserData(
        val model: Map<String, Any>,
        val modelName: String,
        val users: MutableSet<String>
)