package ai.maum.api.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(
        name = "stt_authorized",
        uniqueConstraints = [
            UniqueConstraint(columnNames = ["client_id", "model_id"])
        ]
)
class SttValidEntity: BaseEntity() {
    @Id
    @SequenceGenerator(name = "STT_AUTH_SEQ_GEN", sequenceName = "STT_AUTH_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STT_AUTH_SEQ_GEN")
    var id: Long? = null

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    var client: UserEntity = UserEntity()

    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "model_id", referencedColumnName = "id")
    var model: SttModelEntity = SttModelEntity()

    @Column
    var arrested: Boolean = false
}