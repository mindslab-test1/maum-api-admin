package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.UserEntity

interface ValidRepository {
    fun findByClientAndModel(client: UserEntity, model: Any): Any?
    fun findByClientAndModelAndActiveIsTrue(client: UserEntity, model: Any): Any?
    fun findAllByModelAndActiveIsTrue(modelEntity: Any): MutableList<*>?
}