package ai.maum.api.admin.infra.config

import org.springframework.amqp.core.*
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq")
class RabbitMqConfig{
    lateinit var profix: String
    lateinit var exchanges: List<String>

    @Bean
    fun exchangeMap(): Map<String, FanoutExchange>{
        val exchangeMap = mutableMapOf<String, FanoutExchange>()
        for (exchangeEntry in exchanges) {
            exchangeMap[exchangeEntry.split(":")[0]] =
                    FanoutExchange(exchangeEntry.split(":")[1] + "-$profix")
        }

        return exchangeMap
    }
}