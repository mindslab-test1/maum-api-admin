package ai.maum.api.admin.infra.service

import ai.maum.api.admin.core.model.UpdateMessage
import ai.maum.api.admin.core.repository.ModelAdminRepository
import ai.maum.api.admin.core.service.AdminService
import ai.maum.api.admin.core.service.ServerNotifierService
import ai.maum.api.admin.utils.enum.Engines
import ai.maum.api.admin.utils.json.mapToJsonObject
import ai.maum.api.admin.utils.reflect.EntityHandler
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class AdminServiceImpl(
        val adminRepository: ModelAdminRepository,
        val notifierService: ServerNotifierService,
        val entityHandler: EntityHandler
): AdminService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun modelCreate(modelName: String, creator: String, modelInfo: Map<String, Any?>, engine: Engines) {
        logger.debug("engine: ${engine.value} model create")
        adminRepository.createModel(
                model = modelInfo,
                modelName = modelName,
                creator = creator,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = CREATE,
                        modelName = modelName,
                        modelInfo =  mapToJsonObject(modelInfo),
                        clientAction = ADD,
                        client = creator
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun modelUpdate(modelName: String, modelInfo: Map<String, Any?>, engine: Engines) {
        logger.debug("engine: ${engine.value} model update")
        val updatedEntity = adminRepository.updateModel(
                model = modelInfo,
                modelName = modelName,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        val newModelInfo = entityHandler.modelInfoToMap(updatedEntity, engine)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = UPDATE,
                        modelName = modelName,
                        modelInfo = mapToJsonObject(newModelInfo)
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun modelDelete(modelName: String, engine: Engines) {
        logger.debug("engine: ${engine.value} model delete")
        adminRepository.deleteModel(
                modelName = modelName,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = DELETE,
                        modelName = modelName
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun userAdd(clientId: String, modelName: String, engine: Engines) {
        logger.debug("engine: ${engine.value} add user")
        adminRepository.addUserToModel(
                clientId = clientId,
                modelName = modelName,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = UPDATE,
                        modelName = modelName,
                        client = clientId,
                        clientAction = ADD
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun userArrest(clientId: String, modelName: String, engine: Engines) {
        logger.debug("engine: ${engine.value} arrest user")
        adminRepository.arrestUserFromModel(
                clientId = clientId,
                modelName = modelName,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = UPDATE,
                        modelName = modelName,
                        client = clientId,
                        clientAction = REMOVE
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun userRelease(clientId: String, modelName: String, engine: Engines) {
        logger.debug("engine: ${engine.value} release user")
        adminRepository.releaseUser(
                clientId = clientId,
                modelName = modelName,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = UPDATE,
                        modelName = modelName,
                        client = clientId,
                        clientAction = ADD
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun userRemove(clientId: String, modelName: String, engine: Engines) {
        logger.debug("engine: ${engine.value} remove user")
        adminRepository.removeUserFromModel(
                clientId = clientId,
                modelName = modelName,
                engine = engine
        )
        logger.debug(LOG_UPDATE_MESSAGE)
        notifierService.notifySubscribers(
                message = UpdateMessage(
                        action = UPDATE,
                        modelName = modelName,
                        client = clientId,
                        clientAction = REMOVE
                ),
                engine = engine
        )
        logger.debug(LOG_NOTIFY_MESSAGE)
    }

    override fun getInitData(engine: Engines): Map<String, Any> {
        logger.debug("engine: ${engine.value} init data")
        return mutableMapOf("modelList" to adminRepository.getInitData(engine))
    }

}

const val CREATE = "create"
const val UPDATE = "update"
const val DELETE = "delete"
const val ADD = "add"
const val REMOVE = "remove"

const val LOG_UPDATE_MESSAGE = "successfully updated database"
const val LOG_NOTIFY_MESSAGE = "successfully notified subscribers"