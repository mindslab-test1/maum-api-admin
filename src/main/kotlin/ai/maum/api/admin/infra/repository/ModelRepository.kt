package ai.maum.api.admin.infra.repository

interface ModelRepository {
    fun findByModelNameAndActiveIsTrue(modelName: String): Any?
    fun findAllByActiveIsTrue(): MutableList<*>?

    fun existsByModelNameAndActiveIsTrue(modelName: String): Boolean
}