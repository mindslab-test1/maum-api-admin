package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.boundaries.dto.init.ModelAndUserData
import ai.maum.api.admin.core.repository.ModelAdminRepository
import ai.maum.api.admin.infra.entity.*
import ai.maum.api.admin.utils.const.*
import ai.maum.api.admin.utils.enum.Engines
import ai.maum.api.admin.utils.reflect.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository

@Repository
class ModelAdminRepositoryImpl(
        val userRepository: UserRepository,
        val repositoryHandler: RepositoryHandler,
        val entityHandler: EntityHandler
): ModelAdminRepository {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun getModelInfo(modelName: String, engine: Engines) {
        TODO("Not yet implemented")
    }

    override fun getModelUsers(modelName: String, engine: Engines) {
        TODO("Not yet implemented")
    }

    override fun getInitData(engine: Engines): MutableList<ModelAndUserData> {
        val modelList = repositoryHandler.getModelRepository(engine).findAllByActiveIsTrue() ?: TODO()
        val resultList = mutableListOf<ModelAndUserData>()

        modelList.forEach model@{ modelIter ->
            val validList = repositoryHandler.getValidRepository(engine)
                    .findAllByModelAndActiveIsTrue(modelIter ?: return@model) ?: mutableListOf<Any>()
            val userSet = mutableSetOf<String>()
            validList.forEach valid@{ validIter ->
                val userClientId = (getPropByName(validIter ?: return@valid, PARAM_CLIENT_ENTITY) as UserEntity).clientId
                userSet.add(userClientId)
            }

            resultList.add(ModelAndUserData(
                    model = entityHandler.modelInfoToMap(modelIter, engine),
                    modelName = getPropByName(modelIter, PARAM_MODEL_NAME) as? String ?: "",
                    users = userSet
            ))
        }

        return resultList
    }

    override fun createModel(model: Map<String, Any?>, modelName: String, creator: String, engine: Engines) {
        val modelRepository = repositoryHandler.getModelRepository(engine)
        val validRepository = repositoryHandler.getValidRepository(engine)

        if(modelRepository.existsByModelNameAndActiveIsTrue(modelName)) TODO()
        val creatorEntity = userRepository.findByClientIdAndActiveIsTrue(creator) ?: createUser(creator = creator)
        val modelEntity = entityHandler.createNewModelEntity(model, engine)
        setPropByName(modelEntity, PARAM_MODEL_NAME, modelName)
        logger.debug(modelEntity.toString())
        invokeInterfaceMethod(modelRepository, FUN_SAVE, modelEntity)

        val validEntity = entityHandler.createNewValidEntityInstance(engine)
        setPropByName(validEntity, PARAM_CLIENT_ENTITY, creatorEntity)
        setPropByName(validEntity, PARAM_MODEL_ENTITY, modelEntity)
        logger.debug(validEntity.toString())
        invokeInterfaceMethod(validRepository, FUN_SAVE, validEntity)
    }

    override fun updateModel(model: Map<String, Any?>, modelName: String, engine: Engines): Any {
        val modelRepository = repositoryHandler.getModelRepository(engine)
        val modelEntity = modelRepository.findByModelNameAndActiveIsTrue(modelName) ?: TODO()
        model.entries.forEach { entry ->
            entry.value?.let { setPropByName(modelEntity, entry.key, it) }
        }

        return invokeInterfaceMethod(modelRepository, FUN_SAVE, modelEntity) ?: TODO()
    }

    override fun deleteModel(modelName: String, engine: Engines) {
        val modelRepository = repositoryHandler.getModelRepository(engine)
        val modelEntity = modelRepository.findByModelNameAndActiveIsTrue(modelName) ?: TODO()
        setPropByName(modelEntity, PARAM_ACTIVE, false)

        invokeInterfaceMethod(modelRepository, FUN_SAVE, modelEntity)
    }

    override fun addUserToModel(clientId: String, modelName: String, engine: Engines) {
        val modelRepository = repositoryHandler.getModelRepository(engine)
        val validRepository = repositoryHandler.getValidRepository(engine)
        val userEntity = userRepository.findByClientIdAndActiveIsTrue(clientId) ?: createUser(clientId)

        val modelEntity = modelRepository.findByModelNameAndActiveIsTrue(modelName) ?: TODO()
        val validEntity = validRepository.findByClientAndModel(userEntity, modelEntity) ?: entityHandler.createNewValidEntityInstance(engine)
        if(getPropByName(validEntity, PARAM_ACTIVE) == true){
            setPropByName(validEntity, PARAM_CLIENT_ENTITY, userEntity)
            setPropByName(validEntity, PARAM_MODEL_ENTITY, modelEntity)
        } else {
            setPropByName(validEntity, PARAM_ACTIVE, true)
        }

        invokeInterfaceMethod(validRepository, FUN_SAVE, validEntity)

    }

    override fun arrestUserFromModel(clientId: String, modelName: String, engine: Engines) {
        val userEntity = userRepository.findByClientIdAndActiveIsTrue(clientId) ?: TODO()
        val modelEntity = repositoryHandler.getModelRepository(engine)
                .findByModelNameAndActiveIsTrue(modelName) ?: TODO()
        val validRepository = repositoryHandler.getValidRepository(engine)
        val validEntity = validRepository.findByClientAndModelAndActiveIsTrue(userEntity, modelEntity) ?: TODO()
        setPropByName(validEntity, PARAM_ARRESTED, true)

        invokeInterfaceMethod(validRepository, FUN_SAVE, validEntity)
    }

    override fun releaseUser(clientId: String, modelName: String, engine: Engines) {
        val userEntity = userRepository.findByClientIdAndActiveIsTrue(clientId) ?: TODO()
        val modelEntity = repositoryHandler.getModelRepository(engine)
                .findByModelNameAndActiveIsTrue(modelName) ?: TODO()
        val validRepository = repositoryHandler.getValidRepository(engine)
        val validEntity = validRepository.findByClientAndModelAndActiveIsTrue(userEntity, modelEntity) ?: TODO()
        setPropByName(validEntity, PARAM_ARRESTED, false    )

        invokeInterfaceMethod(validRepository, FUN_SAVE, validEntity)
    }

    override fun removeUserFromModel(clientId: String, modelName: String, engine: Engines) {
        val userEntity = userRepository.findByClientIdAndActiveIsTrue(clientId) ?: TODO()
        val modelEntity = repositoryHandler.getModelRepository(engine)
                .findByModelNameAndActiveIsTrue(modelName) ?: TODO()
        val validRepository = repositoryHandler.getValidRepository(engine)
        val validEntity = validRepository.findByClientAndModelAndActiveIsTrue(userEntity, modelEntity) ?: TODO()
        setPropByName(validEntity, PARAM_ACTIVE, false)

        invokeInterfaceMethod(validRepository, FUN_SAVE, validEntity)
    }

    private fun createUser(creator: String): UserEntity{
        if(userRepository.existsByClientIdAndActiveIsTrue(clientId = creator)) TODO()
        val newClient = UserEntity()
        newClient.clientId = creator
        return userRepository.save(newClient)
    }

}


