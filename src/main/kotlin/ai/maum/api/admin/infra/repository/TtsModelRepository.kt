package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.TtsModelEntity
import org.springframework.data.repository.CrudRepository

interface TtsModelRepository: CrudRepository<TtsModelEntity, Long>, ModelRepository {
    override fun findByModelNameAndActiveIsTrue(modelName: String): TtsModelEntity?
    override fun findAllByActiveIsTrue(): MutableList<TtsModelEntity>?

    override fun existsByModelNameAndActiveIsTrue(modelName: String): Boolean
}