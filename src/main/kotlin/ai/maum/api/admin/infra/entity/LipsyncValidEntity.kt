package ai.maum.api.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(
        name = "lipsync_authorized",
        uniqueConstraints = [
            UniqueConstraint(columnNames = ["client_id", "model_id"])
        ]
)
class LipsyncValidEntity : BaseEntity() {
    @Id
    @SequenceGenerator(name = "LIPS_AUTH_SEQ_GEN", sequenceName = "LIPS_AUTH_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LIPS_AUTH_SEQ_GEN")
    var id: Long? = null

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    var client: UserEntity = UserEntity()

    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "model_id", referencedColumnName = "id")
    var model: LipsyncModelEntity = LipsyncModelEntity()

    @Column
    var arrested: Boolean = false

    override fun toString(): String {
        return "LipsyncValidEntity(id=$id, client=$client, model=$model, arrested=$arrested)"
    }
}