package ai.maum.api.admin.infra.service

import ai.maum.api.admin.core.model.UpdateMessage
import ai.maum.api.admin.core.service.ServerNotifierService
import ai.maum.api.admin.utils.enum.Engines
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.FanoutExchange
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class FanoutMessageSender(
        val template: RabbitTemplate,
        val exchanges: Map<String, FanoutExchange>
): ServerNotifierService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun notifySubscribers(message: UpdateMessage, engine: Engines) {
        logger.debug("send message inferred: $message")
        template.convertAndSend(
                exchanges[engine.value]?.name ?: throw ResponseStatusException(HttpStatus.NOT_IMPLEMENTED, "Engine Exchange Not Defined"),
                "",
                Json{ encodeDefaults = true }.encodeToString(message)
        )
        logger.debug("sent message")
    }
}