package ai.maum.api.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "stt_model_info")
class SttModelEntity : BaseEntity(){
    @Id
    @SequenceGenerator(name = "STT_MODEL_SEQ_GEN", sequenceName = "STT_MODEL_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STT_MODEL_SEQ_GEN")
    var id: Long? = null

    @Column
    var modelName: String = ""

    @Column
    var host: String = ""

    @Column
    var port: Long = 0

    @Column
    var sampleRate: Long = 8000

    @Column
    var lang: String = "ko_KR"

    @Column
    var open: Boolean = false
}