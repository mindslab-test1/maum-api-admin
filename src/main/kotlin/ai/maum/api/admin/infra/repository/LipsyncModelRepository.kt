package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.LipsyncModelEntity
import org.springframework.data.repository.CrudRepository

interface LipsyncModelRepository: CrudRepository<LipsyncModelEntity, Long>, ModelRepository {
    override fun findByModelNameAndActiveIsTrue(modelName: String): LipsyncModelEntity?
    override fun findAllByActiveIsTrue(): MutableList<LipsyncModelEntity>?

    override fun existsByModelNameAndActiveIsTrue(modelName: String): Boolean
}