package ai.maum.api.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "lipsync_model_info")
class LipsyncModelEntity : BaseEntity() {
    @Id
    @SequenceGenerator(name = "LIPS_MODEL_SEQ_GEN", sequenceName = "LIPS_MODEL_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LIPS_MODEL_SEQ_GEN")
    var id: Long? = null

    @Column
    var modelName: String = ""

    @Column
    var host: String = ""

    @Column
    var port: Long = 0

    @Column
    var open: Boolean = false

    override fun toString(): String {
        return "LipsyncModelEntity(id=$id, modelName='$modelName', host='$host', port=$port, open=$open)"
    }
}