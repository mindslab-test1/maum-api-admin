package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.SttValidEntity
import ai.maum.api.admin.infra.entity.UserEntity
import org.springframework.data.repository.CrudRepository

interface SttValidRepository: CrudRepository<SttValidEntity, Long>, ValidRepository {
    override fun findByClientAndModel(client: UserEntity, model: Any): SttValidEntity?
    override fun findByClientAndModelAndActiveIsTrue(client: UserEntity, model: Any): SttValidEntity?
    override fun findAllByModelAndActiveIsTrue(modelEntity: Any): MutableList<SttValidEntity>?
}