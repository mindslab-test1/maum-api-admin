package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.UserEntity
import ai.maum.api.admin.infra.entity.LipsyncValidEntity
import org.springframework.data.repository.CrudRepository

interface LipsyncValidRepository: CrudRepository<LipsyncValidEntity, Long>, ValidRepository {
    override fun findByClientAndModel(client: UserEntity, model: Any): LipsyncValidEntity?
    override fun findByClientAndModelAndActiveIsTrue(client: UserEntity, model: Any): LipsyncValidEntity?
    override fun findAllByModelAndActiveIsTrue(modelEntity: Any): MutableList<LipsyncValidEntity>?
}