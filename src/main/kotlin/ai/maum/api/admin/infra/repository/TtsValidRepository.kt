package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.UserEntity
import ai.maum.api.admin.infra.entity.TtsValidEntity
import org.springframework.data.repository.CrudRepository

interface TtsValidRepository: CrudRepository<TtsValidEntity, Long>, ValidRepository {
    override fun findByClientAndModel(client: UserEntity, model: Any): TtsValidEntity?
    override fun findByClientAndModelAndActiveIsTrue(client: UserEntity, model: Any): TtsValidEntity?
    override fun findAllByModelAndActiveIsTrue(modelEntity: Any): MutableList<TtsValidEntity>?
}