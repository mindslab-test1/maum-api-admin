package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.UserEntity
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<UserEntity, Long> {
    fun findByClientIdAndActiveIsTrue(clientId: String): UserEntity?

    fun existsByClientIdAndActiveIsTrue(clientId: String): Boolean
}