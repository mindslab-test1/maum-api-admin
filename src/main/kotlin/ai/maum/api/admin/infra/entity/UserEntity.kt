package ai.maum.api.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*


@Entity
@DynamicUpdate
@Table(name = "user_info")
class UserEntity: BaseEntity() {
    @Id
    @SequenceGenerator(name = "USER_SEQ_GEN", sequenceName = "USER_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ_GEN")
    var id: Long? = null

    @Column(unique = true)
    var clientId: String = ""

    override fun toString(): String {
        return "UserEntity(id=$id, clientId='$clientId')"
    }
}