package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.SttModelEntity
import org.springframework.data.repository.CrudRepository

interface SttModelRepository: CrudRepository<SttModelEntity, Long>, ModelRepository {
    override fun findByModelNameAndActiveIsTrue(modelName: String): SttModelEntity?
    override fun findAllByActiveIsTrue(): MutableList<SttModelEntity>?

    override fun existsByModelNameAndActiveIsTrue(modelName: String): Boolean
}