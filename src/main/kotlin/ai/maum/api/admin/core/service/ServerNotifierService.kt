package ai.maum.api.admin.core.service

import ai.maum.api.admin.core.model.UpdateMessage
import ai.maum.api.admin.utils.enum.Engines

interface ServerNotifierService {
    fun notifySubscribers(message: UpdateMessage, engine: Engines)
}