package ai.maum.api.admin.core.usecase

import ai.maum.api.admin.boundaries.dto.user.UserUpdateDto
import ai.maum.api.admin.core.service.AdminService
import ai.maum.api.admin.utils.enum.Engines
import org.springframework.stereotype.Component

@Component
class UserCrudUseCase(
        val adminService: AdminService,
) {
    fun userAdd(dto: UserUpdateDto, engine: Engines){
        adminService.userAdd(dto.clientId, dto.modelName, engine)
    }

    fun userArrest(dto: UserUpdateDto, engine: Engines){
        adminService.userArrest(dto.clientId, dto.modelName, engine)
    }

    fun userRelease(dto: UserUpdateDto, engine: Engines){
        adminService.userRelease(dto.clientId, dto.modelName, engine)
    }

    fun userRemove(dto: UserUpdateDto, engine: Engines){
        adminService.userRemove(dto.clientId, dto.modelName, engine)
    }
}