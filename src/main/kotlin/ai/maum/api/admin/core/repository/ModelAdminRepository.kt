package ai.maum.api.admin.core.repository

import ai.maum.api.admin.boundaries.dto.init.ModelAndUserData
import ai.maum.api.admin.utils.enum.Engines

interface ModelAdminRepository {
    fun getModelInfo(modelName: String, engine: Engines)
    fun getModelUsers(modelName: String, engine: Engines)

    fun getInitData(engine: Engines): MutableList<ModelAndUserData>

    fun createModel(model: Map<String, Any?>, modelName: String, creator: String, engine: Engines)
    fun updateModel(model: Map<String, Any?>, modelName: String, engine: Engines): Any
    fun deleteModel(modelName: String, engine: Engines)

    fun addUserToModel(clientId: String, modelName: String, engine: Engines)
    fun arrestUserFromModel(clientId: String, modelName: String, engine: Engines)
    fun releaseUser(clientId: String, modelName: String, engine: Engines)
    fun removeUserFromModel(clientId: String, modelName: String, engine: Engines)
}