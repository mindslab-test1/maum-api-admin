package ai.maum.api.admin.core.usecase

import ai.maum.api.admin.boundaries.dto.model.ModelCreateDto
import ai.maum.api.admin.boundaries.dto.model.ModelDeleteDto
import ai.maum.api.admin.boundaries.dto.model.ModelUpdateDto
import ai.maum.api.admin.core.service.AdminService
import ai.maum.api.admin.utils.enum.Engines
import org.springframework.stereotype.Service

@Service
class ModelCrudUseCase(
        val adminService: AdminService
) {
    fun modelCreate(dto: ModelCreateDto, engine: Engines){
        adminService.modelCreate(dto.modelName, dto.creator, dto.modelInfo, engine)
    }

    fun modelUpdate(dto: ModelUpdateDto, engine: Engines){
        adminService.modelUpdate(dto.modelName, dto.modelInfo, engine)
    }

    fun modelDelete(dto: ModelDeleteDto, engine: Engines){
        adminService.modelDelete(dto.modelName, engine)
    }

    fun getInitData(engines: Engines): Map<String, Any>{
        return adminService.getInitData(engines)
    }
}