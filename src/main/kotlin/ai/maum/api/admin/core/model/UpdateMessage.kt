package ai.maum.api.admin.core.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
data class UpdateMessage(
        val action: String? = "none",
        val modelName: String? = "",
        val modelInfo: JsonObject? = null,
        val clientAction: String? = "none",
        val client: String? = null
)