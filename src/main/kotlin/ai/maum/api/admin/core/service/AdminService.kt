package ai.maum.api.admin.core.service

import ai.maum.api.admin.utils.enum.Engines

interface AdminService {
    fun modelCreate(modelName: String, creator: String, modelInfo: Map<String, Any?>, engine: Engines)
    fun modelUpdate(modelName: String, modelInfo: Map<String, Any?>, engine: Engines)
    fun modelDelete(modelName: String, engine: Engines)

    fun userAdd(clientId: String, modelName: String, engine: Engines)
    fun userArrest(clientId: String, modelName: String, engine: Engines)
    fun userRelease(clientId: String, modelName: String, engine: Engines)
    fun userRemove(clientId: String, modelName: String, engine: Engines)

    fun getInitData(engine: Engines): Map<String, Any>
}