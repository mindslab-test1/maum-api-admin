package ai.maum.api.admin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
class ApiAdminApplication

fun main(args: Array<String>) {
    runApplication<ApiAdminApplication>(*args)
}
