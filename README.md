# API 2.0 Admin
API 2.0 중 API서버의 internal model을 관리하기 위한 admin 서버입니다.  
개별 MSA의 API들은 실제로 존재하는 사용자인지에 대한 정보를 제공받지 않고 어떤 사용자가 사용 가능한지에 대한 정보만 가지고 있을 수 있도록 제작합니다.  
각 엔진별 API들에 연결할 exchange를 엔진별로 별도 관리, 엔진 API서버는 자신이 가질 수 있는 데이터만 신경쓰도록 진행하도록 합니다.  
model / user crud에 관한 API Endpoint를 한 boundary에 두고, header에 엔진에 대한 정보를 담아서 use case에서 구분도록 제작되었습니다.  
## init data
API서버 container(Kubernetes 내부의 POD)에서 초기 데이터를 요청하는 endpoint.
## model crud
사용가능한 모델 정보를 Admin DB에 저장하고, 그에 대한 message를 produce하는 endpoint.  
DTO의 modelInfo를 일반적인 parsible json인 `Map<String, Any?>` 타입으로 지정.  
엔진이 추가될때 `Map.Entry.key`의 값에 대한 validation method 구현 필요.  
추가로 현재 사용중인 `Spring-Boot` 버전을 기준으로 `kotlinx.serialization` 라이브러리가 아직 미지원이기 때문에, 구현체 내부가 조금 지저분함. 추후 조치가 필요.
### model create
Request Method PUT
### model update
Request Method POST
### model delete
Request Method DELETE
## user crud
모델에 대해서 사용 가능한 사용자를 Admin DB에 저장하고, 그에 대한 message를 produce하는 endpoint.  
UserEntity를 하나로 관리하도록 변경 필요.  
### user add
Request Method PUT
### user arrest
Request Method POST
### user release
Request Method POST
### user remove
Request Method DELETE
