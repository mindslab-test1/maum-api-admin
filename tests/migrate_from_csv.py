import csv

from model_add import ModelCreater
from user_add import UserAdd


class ModelMigrationCollection:
    def __init__(
            self,
            admin_address: str = "localhost:8883",
            auth_token: str = ""
    ):
        self.model_dict = {}
        self.admin_address = admin_address
        self.auth_token = auth_token

    def add_model(
            self,
            model_name: str,
            model_user: str,
            model_host: str,
            model_port: int,
            model_samplerate: int,
            model_speaker: int,
            model_lang: str
    ):
        if model_name in self.model_dict.keys():
            self.model_dict[model_name]["users"].append(model_user)
        else:
            self.model_dict[model_name] = {}
            self.model_dict[model_name]["model_info"] = {
                "ip": model_host,
                "port": model_port,
                "sampleRate": model_samplerate,
                "speakerId": model_speaker,
                "lang": model_lang,
                "open": False
            }
            self.model_dict[model_name]["users"] = [model_user]

    def migrate(self):
        model_add = ModelCreater(admin_address=self.admin_address, auth_token=self.auth_token)
        user_add = UserAdd(admin_address=self.admin_address, auth_token=self.auth_token)
        for model_name in self.model_dict.keys():
            model_info = self.model_dict[model_name]["model_info"]
            model_users = self.model_dict[model_name]["users"]
            model_creator = model_users[0]

            model_add.set_model(
                model_name=model_name,
                model_host=model_info["ip"],
                model_port=model_info["port"],
                model_samplerate=model_info["sampleRate"],
                model_speaker=model_info["speakerId"],
                model_lang=model_info["lang"],
                model_open=model_info["open"],
                model_creator=model_creator
            )
            response = model_add.send_request()
            if response.status_code != 200:
                print("error for model: {}".format(model_name))

            if len(model_users) == 1:
                continue
            model_added_users = model_users[1:]
            for user in model_added_users:
                user_add.set_user(
                    client_id=user,
                    model_name=model_name
                )
                response = user_add.send_request()
                if response.status_code != 200:
                    print(response.json())
                    print("error adding user: {} to model: {}".format(user, model_name))

    def __str__(self):
        return str(self.model_dict)


if __name__ == '__main__':
    with open("models_20201126.csv") as f:
        result = csv.reader(f)
        result_list = []
        for line in result:
            result_list.append(line)

    col_names = result_list[0]
    data = result_list[1:]

    model_data_collection = ModelMigrationCollection(
        admin_address="localhost:8883",
        auth_token="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYXNkZiJdLCJzY29wZSI6WyJzdHQiLCJ0dHMiLCJhdmF0YXIiXSwiZXhwIjoxNjA3NTc2NjA2LCJhdXRob3JpdGllcyI6WyJST0xFX0FQSSIsIlJPTEVfQU1MIiwiUk9MRV9TVEFUUyJdLCJqdGkiOiJkOTYzNmU1OS05MjMxLTQ3NTQtOGZhMy1mN2JhMjdjYjg3YmUiLCJjbGllbnRfaWQiOiI2Y2Q1NWU5OS1hOWNjLTRjMDItYjhlMS00MjgwMGU3MGIwZjcifQ.cjVU5N9qeKHTeE3pNq8s8NA4-GZW1k-BBTUH8hiuQAhWN4Ana-oUlpPp0wjqdTKw87lBrle-CGZNXViaIVZBhfe6HI4w43XNSwKKZefPuKcOfcdb9ZB_ZJaD_1MJ6IFZI3OH391pCcqt45XTkuDb9vtngMbzQb8eESlSwS_j5GbCXcmH5pCBcXLGbB6cpNsSvK8U8qXuzgFnrvJYcp_TUXFw-UruW3KbRqftX3iOUgd9dlv18Pe9DOJgTBS4IhdwJ7syxzpGGceGqMYwWWB8vWM1uCnLuwtlSwZFXNnZ6HzXp965KJkOhHl9a0WaoaIyOCJJ_dGodMdLP8zT4nlCtA"
    )
    for data in data:
        model_data_collection.add_model(
            model_name=data[list(col_names).index("voiceName")],
            model_user=data[list(col_names).index("apiId")],
            model_host=data[list(col_names).index("ip")],
            model_port=int(data[list(col_names).index("port")]),
            model_samplerate=int(data[list(col_names).index("port")]),
            model_speaker=int(data[list(col_names).index("speaker")]),
            model_lang=data[list(col_names).index("lang")]
        )
    model_data_collection.migrate()
