import requests


class UserAdd:
    def __init__(
            self,
            admin_address: str = "localhost:8883",
            client_id: str = "test_client",
            model_name: str = "test_model_name",
            auth_token: str = ""
    ):
        self.admin_address = admin_address
        self.client_id = client_id
        self.model_name = model_name
        self.auth_token = auth_token

    def set_user(
            self,
            client_id: str = "test_client",
            model_name: str = "test_model_name"
    ):
        self.client_id = client_id
        self.model_name = model_name

    def send_request(self):
        payload = {
            "modelName": self.model_name,
            "clientId": self.client_id
        }

        headers = {
            "Authorization": "Bearer {}".format(self.auth_token),
            "ai-maum-engine": "tts",
            "Content-Type": "application/json"
        }

        response = requests.put(
            "http://{}/admin/api/user/add".format(self.admin_address),
            headers=headers,
            json=payload
        )
        return response


if __name__ == '__main__':
    model_creater = UserAdd()
    model_creater.send_request()
