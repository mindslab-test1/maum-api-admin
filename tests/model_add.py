import requests


class ModelCreater():
    def __init__(
            self,
            admin_address: str = "localhost:8883",
            model_name: str = "test_model_name",
            model_host: str = "127.0.0.1",
            model_port: int = 9999,
            model_samplerate: int = 22050,
            model_lang: str = "ko_KR",
            model_speaker: int = 0,
            model_open: bool = False,
            model_creator: str = "test_creator",
            auth_token: str = ""
    ):
        self.admin_address = admin_address
        self.model_name = model_name
        self.model_host = model_host
        self.model_port = model_port
        self.model_samplerate = model_samplerate
        self.model_lang = model_lang
        self.model_speaker = model_speaker
        self.model_open = model_open
        self.model_creator = model_creator
        self.auth_token = auth_token

    def set_model(
            self,
            model_name: str = "test",
            model_host: str = "127.0.0.1",
            model_port: int = 9999,
            model_samplerate: int = 22050,
            model_lang: str = "ko_KR",
            model_speaker: int = 0,
            model_open: bool = False,
            model_creator: str = "test_creator"
    ):
        self.model_name = model_name
        self.model_host = model_host
        self.model_port = model_port
        self.model_samplerate = model_samplerate
        self.model_lang = model_lang
        self.model_speaker = model_speaker
        self.model_open = model_open
        self.model_creator = model_creator

    def send_request(self):
        payload = {
            "modelName": self.model_name,
            "modelInfo": {
                "host": self.model_host,
                "port": self.model_port,
                "sampleRate": self.model_samplerate,
                "speakerId": self.model_speaker,
                "lang": self.model_lang,
                "open": self.model_open
            },
            "creator": self.model_creator
        }

        headers = {
            "Authorization": "Bearer {}".format(self.auth_token),
            "ai-maum-engine": "tts",
            "Content-Type": "application/json"
        }

        response = requests.put(
            "http://{}/admin/api/model/create".format(self.admin_address),
            headers=headers,
            json=payload
        )
        return response


if __name__ == '__main__':
    model_creater = ModelCreater(
        admin_address="114.108.173.106:8883"
    )
    model_creater.send_request()
